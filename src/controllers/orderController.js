const { where } = require('sequelize')
const { models } = require('../models/index')
const response = require('../utils/response')

const orderFood = async (req, res) => {
    try {
        let { user_id, food_id, amount, code } = req.body
        code = code || ''
        if (!user_id || !food_id || !amount) {
            response.failCode(res, null, 'Điền đầy đủ thông tin')
            return
        }
        let user = await models.user.findByPk(user_id)
        if (!user) {
            response.failCode(res, null, 'Người dùng không tồn tại')
            return
        }
        let food = await models.food.findByPk(food_id)
        if (!food) {
            response.failCode(res, null, 'Món ăn không tồn tại')
            return
        }
        let sub_food = await models.food.findAll({
            include: 'sub_foods',
            where: {
                food_id,
            },
        })
        let arr_sub_id = []
        let sub_food_json = JSON.stringify(sub_food)
        let sub_foods = JSON.parse(sub_food_json)[0].sub_foods
        if (sub_foods.length > 0) {
            arr_sub_id = sub_foods.map((e) => e?.sub_id)
        }
        console.log(arr_sub_id)

        let result = await models.order.create({
            user_id,
            food_id,
            amount,
            code,
            arr_sub_id: JSON.stringify(arr_sub_id),
        })
        response.successCode(res, result, 'Đặt món thành công')
    } catch (err) {
        response.failCode(res, null, 'Người dùng đã đặt món ăn này')
    }
}

module.exports = {
    orderFood,
}
