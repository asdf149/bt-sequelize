const { models } = require('../models/index')
const response = require('../utils/response')

const likeRes = async (req, res) => {
    try {
        let obj = {
            user_id: req.body?.user_id,
            res_id: req.body?.res_id,
            date_like: Date.now(),
        }
        let msg
        let result = await models.like_res.findOne({
            where: {
                user_id: obj.user_id,
                res_id: obj.res_id,
            },
        })
        if (result) {
            result = await models.like_res.destroy({
                where: {
                    user_id: obj.user_id,
                    res_id: obj.res_id,
                },
            })
            if (result) msg = 'Unlike thành công'
        } else {
            result = await models.like_res.create(obj)
            if (result) msg = 'Like thành công'
        }
        response.successCode(res, result, msg)
    } catch (err) {
        response.failCode(res, err, 'Lỗi')
    }
}

const likeListByRes = async (req, res) => {
    let result = await models.restaurant.findAll({ include: 'user_id_users' })
    response.successCode(res, result)
}
const likeListByUser = async (req, res) => {
    let result = await models.user.findAll({ include: 'res_id_restaurants' })
    response.successCode(res, result)
}

const likeByUser = async (req, res) => {
    let { user_id } = req.params
    let result = await models.user.findAll({
        include: 'res_id_restaurants',
        where: { user_id },
    })
    response.successCode(res, result)
}
const likeByRes = async (req, res) => {
    let { res_id } = req.params
    let result = await models.restaurant.findAll({
        include: 'user_id_users',
        where: { res_id },
    })
    response.successCode(res, result)
}

module.exports = {
    likeRes,
    likeListByRes,
    likeListByUser,
    likeByUser,
    likeByRes,
}
