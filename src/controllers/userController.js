const { models } = require('../models/index')
const response = require('../utils/response')

const getUser = async (req, res) => {
    let result = await models.user.findAll()
    response.successCode(res, result, 'Thành công')
}

const storeUser = async (req, res) => {
    try {
        let { full_name, email, password } = req.body
        let newUser = { full_name, email, password }
        let result = await models.user.create(newUser)
        console.log(result)
        response.successCode(res, result, 'Tạo tài khoản thành công')
    } catch (err) {
        response.failCode(res, err, 'Tạo tài khoản thất bại')
    }
}

const updateUser = async (req, res) => {
    try {
        let { id } = req.params
        let findId = await models.user.findByPk(id)
        if (!findId) response.failCode(res, null, 'Không tìm thấy tài khoản')
        let result = await models.user.update(req.body, {
            where: { user_id: id },
        })
        result[0]
            ? response.successCode(res, null, 'Cập nhật thông tin thành công')
            : response.failCode(res, null, 'Cập nhật thông tin thất bại')
    } catch (error) {
        response.failCode(res, error, 'Cập nhật thông tin thất bại')
    }
}

module.exports = {
    getUser,
    storeUser,
    updateUser,
}
