const { where } = require('sequelize')
const { models } = require('../models/index')
const response = require('../utils/response')

const rateRes = async (req, res) => {
    try {
        let { user_id, res_id, amount } = req.body
        let result = null
        let [rate, newCreate] = await models.rate_res.findOrCreate({
            where: { user_id, res_id },
            defaults: {
                user_id,
                res_id,
                amount,
                date_rate: Date.now(),
            },
        })
        if (rate) {
            result = await models.rate_res.update(
                { amount, date_rate: Date.now() },
                {
                    where: {
                        user_id,
                        res_id,
                    },
                }
            )
        }
        if (rate || newCreate) {
            response.successCode(res, null, 'Đánh giá thành công')
        } else {
            response.failCode(res, null, 'Đánh giá thất bại')
        }
    } catch (err) {
        response.failCode(res, err, 'Đánh giá thất bại')
    }
}

const rateListByRes = async (req, res) => {
    let result = await models.restaurant.findAll({
        include: 'user_id_user_rate_res',
    })
    response.successCode(res, result)
}
const rateListByUser = async (req, res) => {
    let result = await models.user.findAll({
        include: 'res_id_restaurant_rate_res',
    })
    response.successCode(res, result)
}
const rateByRes = async (req, res) => {
    let { res_id } = req.params
    let result = await models.restaurant.findAll({
        include: 'user_id_user_rate_res',
        where: {
            res_id,
        },
    })
    response.successCode(res, result)
}
const rateByUser = async (req, res) => {
    let { user_id } = req.params
    let result = await models.user.findAll({
        include: 'res_id_restaurant_rate_res',
        where: {
            user_id,
        },
    })
    response.successCode(res, result)
}

module.exports = {
    rateRes,
    rateListByRes,
    rateListByUser,
    rateByRes,
    rateByUser,
}
