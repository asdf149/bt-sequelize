const { models } = require('../models/index')
const response = require('../utils/response')

const storeFood = (req, res) => {}

const getFood = async (req, res) => {
    let result = await models.food.findAll({ include: 'type' })
    response.successCode(res, result, 'Thành công')
}

module.exports = {
    storeFood,
    getFood,
}
