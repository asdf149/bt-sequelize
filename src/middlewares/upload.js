const multer = require('multer')

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/img')
    },
    filename: (req, file, cb) => {
        const fileNameNew = Date.now() + '_' + file.originalname
        cb(null, fileNameNew)
    },
})

module.exports = storage
