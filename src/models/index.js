const { Sequelize } = require('sequelize')
require('dotenv').config()
const initModels = require('./init-models')

const sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USERNAME,
    process.env.DB_PASS,
    {
        host: process.env.DB_HOST,
        dialect: 'mysql',
        logging: false,
    }
)

const models = initModels(sequelize)

module.exports = { models, sequelize }
