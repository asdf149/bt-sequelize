const express = require('express')
const app = express()
var cors = require('cors')
require('dotenv').config()

const rootRouter = require('./routes/index')

app.use(cors())
app.use(express.json())
app.use(express.static('.'))
app.use(rootRouter)

const PORT = process.env.PORT || 6000

app.listen(PORT, console.log(`chạy tại port ${PORT}`))
