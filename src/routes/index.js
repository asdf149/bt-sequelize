const express = require('express')
const rootRouter = express.Router()
const userRouter = require('./userRouter')
const foodRouter = require('./foodRouter')
const likeRouter = require('./likeRouter')
const rateRouter = require('./rateRouter')
const orderRouter = require('./orderRouter')

rootRouter.use('/user', userRouter)
rootRouter.use('/food', foodRouter)
rootRouter.use('/like', likeRouter)
rootRouter.use('/rate', rateRouter)
rootRouter.use('/order', orderRouter)

module.exports = rootRouter
