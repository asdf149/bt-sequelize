const express = require('express')
const foodRouter = express.Router()
const Food = require('../controllers/foodController')

foodRouter.get('/getFood', Food.getFood)

module.exports = foodRouter
