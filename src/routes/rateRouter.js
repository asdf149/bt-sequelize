const express = require('express')
const rateRouter = express.Router()
const Rate = require('../controllers/rateController')

rateRouter.post('/rateRes', Rate.rateRes)
rateRouter.get('/rateByRes', Rate.rateListByRes)
rateRouter.get('/rateByUser', Rate.rateListByUser)
rateRouter.get('/rateByRes/:res_id', Rate.rateByRes)
rateRouter.get('/rateByUser/:user_id', Rate.rateByUser)

module.exports = rateRouter
