const express = require('express')
const userRouter = express.Router()
const User = require('../controllers/userController')

userRouter.get('/getUser', User.getUser)
userRouter.post('/add-user', User.storeUser)
userRouter.put('/:id', User.updateUser)

module.exports = userRouter
