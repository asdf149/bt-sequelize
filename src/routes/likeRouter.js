const express = require('express')
const likeRouter = express.Router()
const Like = require('../controllers/likeController')

likeRouter.post('/likeRes', Like.likeRes)
likeRouter.get('/likeByRes', Like.likeListByRes)
likeRouter.get('/likeByUser', Like.likeListByUser)
likeRouter.get('/likeByUser/:user_id', Like.likeByUser)
likeRouter.get('/likeByRes/:res_id', Like.likeByRes)

module.exports = likeRouter
