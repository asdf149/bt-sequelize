const express = require('express')
const orderRouter = express.Router()
const Order = require('../controllers/orderController')

orderRouter.post('/orderFood', Order.orderFood)

module.exports = orderRouter
